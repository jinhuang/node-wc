'use strict';

const util = require('util');
const Promise = require('bluebird');
const execAsync = Promise.promisify(require('child_process').exec);


const wc = require('../wc.js');
console.log = jasmine.createSpy('log');

describe('Option validation', function() {
  const legalCases = [
    {
      name: 'should pass validating empty options',
      opts: ''
    },
    {
      name: 'should pass validating a single legal UNIX option',
      opts: '-c'
    },
    {
      name: 'should pass validating multiple legal UNIX options',
      opts: '-cwm'
    },
    {
      name: 'should pass validating a single legal GNU option',
      opts: '--lines'
    },
    {
      name: 'should pass validating multiple legal GNU options',
      opts: '--bytes --words'
    },
    {
      name: 'should pass validating multiple mix legal options',
      opts: '-c --lines -m --max-line-length'
    },
  ];
  
  const illegalCases = [
    {
      name: 'should prompt a single illegal UNIX option', 
      opts: '-s', prompt: 's'
    },
    {
      name: 'should prompt the first among multiple illegal UNIX options', 
      opts: '-foobar', prompt: 'f'
    },
    {
      name: 'should prompt first illegal UNIX options mixed with legal options', 
      opts: '-cmf', prompt: 'f'
    },
    {
      name: 'should prompt a single illegal GNU option',
      opts: '--foobar', prompt: 'foobar'
    },
    {
      name: 'should prompt the first among multiple illegal GNU options',
      opts: '--foobar --snow', prompt: 'foobar'
    },
    {
      name: 'should prompt first illegal GNU options mixed with legal options',
      opts: '--bytes --foobar', prompt: 'foobar'
    },
    {
      name: 'should prompt first illegal option (UNIX)',
      opts: '-cm --lines -Lfd', prompt: 'f'
    },
    {
      name: 'should prompt first illegal option (GNU)',
      opts: '--bytes -ml --foobar --max-line-length', prompt: 'foobar'
    },
  ];
  
  legalCases.forEach(function(legalCase) {
    it(legalCase.name, function() {
      const valid = wc.validate(wc.parse(legalCase.opts.split(' ')));
      expect(valid).toEqual(true);
    });
  });
  
  illegalCases.forEach(function(illegalCase) {
    it(illegalCase.name, function() {
      const prompt = illegalCase.prompt;
      const valid = wc.validate(wc.parse(illegalCase.opts.split(' ')));
      expect(console.log).toHaveBeenCalledWith(wc.promptIllegal(prompt));
      expect(console.log).toHaveBeenCalledWith(wc.promptUsage());
      expect(valid).toEqual(false);      
    });
  });
  
});

describe('Genrating options', function() {
  const testCases = [
    {
      'name': 'should generate correct options for UNIX options',
      'input': '-cwl', 
      'output': {byte: true, word: true, line: true}
    },
    {
      'name': 'should generate correct options for GNU options',
      'input': '--words --max-line-length', 
      'output': {word: true, maxLength: true}
    },
    {
      'name': 'should generate correct options for mixed options',
      'input': '--words -wcm --max-line-length',
      'output': {word: true, byte: true, char: true, maxLength: true}
    },
  ];
  
  testCases.forEach(function(testCase) {
    it(testCase.name, function() {
      const parsedInput = wc.parse(testCase.input.split(' '));
      const result = wc.generateOpts(parsedInput);
      expect(result).toEqual(testCase.output);
    });
  });
});

describe('Processing files', function() {
  
});

describe('Counting content', function() {
  const testCases = [
    {
      'name': 'should count correct words (zero)',
      'content': '\t\t\n\n\n',
      'opts': {word: true},
      'counter': {word: 0}
    },    
    {
      'name': 'should count correct words (non-zero)',
      'content': 'foo bar\n',
      'opts': {word: true},
      'counter': {word: 2}
    },
    {
      'name': 'should count correct lines (zero)',
      'content': 'foobar',
      'opts': {line: true},
      'counter': {line: 0}
    },    
    {
      'name': 'should count correct lines (none-zero)',
      'content': 'foobar\n\n\n',
      'opts': {line: true},
      'counter': {line: 3}
    },
    {
      'name': 'should count correct characters (zero)',
      'content': '',
      'opts': {char: true},
      'counter': {char: 0}
    },
    {
      'name': 'should count correct characters (non-zero)',
      'content': 'foobar',
      'opts': {char: true},
      'counter': {char: 6}
    },
    {
      'name': 'should count correct bytes (zero)',
      'content': '',
      'opts': {byte: true},
      'counter': {byte: 0}
    },
    {
      'name': 'should count correct bytes (non-zero)',
      'content': 'foobar',
      'opts': {byte: true},
      'counter': {byte: 6}
    },   
    {
      'name': 'should count correct max line length (zero)',
      'content': '\n\n\n\n',
      'opts': {maxLength: true},
      'counter': {maxLength: 0}
    },    
    {
      'name': 'should count correct max line length (non-zero)',
      'content': 'foo bar \t is\t\t awesome\n\t\n\n\t\n',
      'opts': {maxLength: true},
      'counter': {maxLength: 43}
    },      
    {
      'name': 'should count correct for multiple metrics',
      'content': 'foo bar \t is\t\t awesome\n\t\n\n\t\n',
      'opts': {word: true, line: true, maxLength: true},
      'counter': {word: 4, line: 4, maxLength: 43}
    },
  ];
  
  testCases.forEach(function(testCase) {
    it(testCase.name, function() {
      const result = wc.countContent(testCase.content, testCase.opts);
      expect(result.counter).toEqual(testCase.counter);      
    });
  });
});

describe('Formatting output', function() {
  const testCases = [
    {
      'name': 'should return undefined for empty entries',
      'entries': [],
      'output': undefined
    },
    {
      'name': 'should format invalid entries correctly (single file not found)',
      'entries': [{file: 'foobar', error: 0}],
      'output': wc.promptFileNotFound('foobar')
    },
    {
      'name': 'should format invalid entries correctly (single directory)',
      'entries': [{file: 'foobar', error: 1}],
      'output': `${wc.promptDirectory('foobar')}`
    },
    {
      'name': 'should format invalid entries correctly (multiple mixed)',
      'entries': [{file: 'foobar', error: 0}, {file: 'snow', error: 1}],
      'output': `${wc.promptFileNotFound('foobar')}\n${wc.promptDirectory('snow')}`
    },
    {
      'name': 'should format valid entries correctly (single count, single entry)',
      'entries': [{file: 'foobar', counter: {word: 1}}],
      'output': '1 foobar'
    }, 
    {
      'name': 'should format valid entries correctly (single count, multiple entries)',
      'entries': [{file: 'foo', counter: {word: 1}}, {file: 'bar', counter: {word: 1}}],
      'output': '1 foo\n1 bar\n2 total'
    },
    {
      'name': 'should format valid entries correctly (multiple counts, single entry)',
      'entries': [{file: 'foobar', counter: {char: 6, word: 1, line: 0}}],
      'output': '0 1 6 foobar'
    },
    {
      'name': 'should format valid entries correctly (multiple counts, multiple entries)',
      'entries': [{file: 'foo', counter: {char: 3, byte: 3, word: 1}}, {file: 'bar', counter: {char: 3, word: 1, byte: 3}}],
      'output': '1 3 3 foo\n1 3 3 bar\n2 6 6 total'
    },
    {
      'name': 'should format padded counts correctly (total no-overflow)',
      'entries': [{file: 'foobar', counter: {char: 17}}, {file: 'abc', counter: {char: 3}}],
      'output': '17 foobar\n 3 abc\n20 total'
    },
    {
      'name': 'should format padded counts correctly (total overflow)',
      'entries': [{file: 'foobar', counter: {byte: 6}}, {file: 'snow', counter: {byte: 4}}],
      'output': ' 6 foobar\n 4 snow\n10 total'
    },
    {
      'name': 'should format mixed correct and incorrect entries correctly',
      'entries': [
        {file: 'foobar', counter: {word: 1, maxLength: 6, line: 0}},
        {file: 'abc', error: 0},
        {file: 'snow', counter: {word: 1, maxLength: 4, line: 1}},
        {file: 'iamadirectory', error: 1}
      ],
      'output': `0 1 6 foobar\n${wc.promptFileNotFound('abc')}\n1 1 4 snow\n${wc.promptDirectory('iamadirectory')}\n0 0 0 iamadirectory\n1 2 6 total`
    }
  ];
  
  testCases.forEach(function(testCase) {
    it(testCase.name, function() {
      const result = wc.formatOutput(testCase.entries);
      expect(result).toEqual(testCase.output);
    });
  });
});

describe('Executing wc', function() {
  const testCases = [
    {
      'name': 'should count correct for specified files (single file)',
      'commandWC': 'wc package.json',
      'commandNode': 'node wc.js package.json'
    },
    {
      'name': 'should count correct for specified files (multiple files)',
      'commandWC': 'wc *.js',
      'commandNode': 'node wc.js *.js',
    },
    {
      'name': 'should count correct for standard in input',
      'commandWC': 'wc < package.json',
      'commandNode': 'node wc.js < package.json'
    },
    {
      'name': 'should count correct for a file list from standard in',
      'commandWC': "find . -name '*.js' -print0 | wc --files0-from=- | tail -n1",
      'commandNode': "find . -name '*.js' -print0 | node wc.js --files0-from=- | tail -n1"
    },
    {
      'name': 'should count correct for a file list from the specified file',
      'commandWC': 'wc --files0-from=filesFrom',
      'commandNode': 'node wc.js --files0-from=filesFrom'
    }
  ]
  
  function count2Array(count) {
    return count.split(/\s+/).map(function(ele) {
      return ele.trim();
    }).filter(function(ele) {
      return ele.length;
    });
  }
  
  testCases.forEach(function(testCase) {
    it(testCase.name, function(done) {
      const promiseWc = execAsync(testCase.commandWC)
        .then(function(stdout, stderr) {
          return stdout;
        }).catch(function(){
          return {};
        });
      const promiseNode = execAsync(testCase.commandNode)
        .then(function(stdout, stderr) {
          return stdout;
        });
      Promise.all([
        promiseWc, promiseNode
      ]).then(function(outputs) {
        const linesWc = outputs[0].split(/\n/);
        const linesNode = outputs[1].split(/\n/);
        expect(linesWc.length).toEqual(linesNode.length);
        linesWc.forEach(function(wcLine, i) {
          const nodeLine = linesNode[i];
          const arrayWc = count2Array(wcLine);
          const arrayNode = count2Array(nodeLine);
          arrayWc.forEach(function(ele, j) {
            expect(ele).toEqual(arrayNode[j]);
          });
        });
        done();
      });
    });
  });
});
