NodeJS implementation of GNU wc
----
### Differences from `wc (GNU coreutils)`
1. When padding the counts in columns, the width is decided by the widest count number in all count field (__including the `total` count when present__); `wc (GNU coreutils) 8.25` appears to decide width without considering this `total` count.
3. __Only supports `UTF-8` encoding__, therefore the counts `bytes` and `chars` are always equal. Could extend to support other encodings/locales by reading `process.env` envrionment variables from the command line just like the `wc (GNU coreutils)`. Yet for the ease of automatic testing, this feature is skipped.

### Installation and Running
It relies on `NodeJS` and two node packages `bluebird` and `minimist`. To run the program, run the following `bash command`
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
source ~/.bashrc
nvm install 5.11.2
git clone https://gitlab.com/jinhuang/node-wc.git
cd node-wc
npm install
node wc.js --help
```

### To Run Test Case
After `npm install`, run `npm test`; the _last two tests_ in `Executing wc` suit may not pass:
1. The first one tests a file list from piped standard in, the `GNU wc` command however produces an rejected promise when the given path is a directory
2. The second one tests a file list from a local file, may need to change the input to a file that __exists__ on your machine to pass the test, for example, `find . -name '*.js' -print0 > filesFrom`

### Performance
The `NodeJS` implementation is noticeably slower than the `GNU` version when the numebr of input files is large. Though I haven't spent much time on tuning the efficiency of the `NodeJS` version, I wouldn't expect a tuned version would catch up with the `GNU` version anyway.