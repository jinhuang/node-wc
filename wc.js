/*
 * TODO add testing case for the major functions:
 * parse(), validate(), generateOpts(), countContent(), countFile(), countFiles()
 * TODO add full test case for execute
 * TODO support encodings other than utf-8
 * TODO adjust maxLength with consideration of width of characters
 * TODO write readme for instructing on how to run the code
 */

'use strict';

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const parseArgs = require('minimist');

const fileInfo = 'package.json';
const fileHelp = 'help.json';

const validOpts = ['c', 'l', 'm', 'w', 'L', 
  'bytes', 'lines', 'chars', 'words', 'max-line-length', 'help', 'version'];
const optionalInput = 'files0-from';
const inputFromStdinChar = '-';
const tabChar = '\t';
const nullChar = '\0';
const newlineChar = '\n';
const spaceChar = ' ';
const minimistChar = '_';
const newlineRegexp = /\r\n|\r|\n/;
const spaceRegexp = /\s+/;
const notFoundError = 0;
const directoryError = 1;


// TODO support other encodings and locales
const encoding = 'utf-8';

function promptIllegal(firstError) {
  return `wc: invalid option -- '${firstError}'`;
}

function promptUsage() {
  return 'try \'node wc.js --help\' for more information.';
}

function promptFileNotFound(file) {
  return `wc: ${file} no such file or directory`;
}

function promptDirectory(file) {
  return `wc: ${file}: Is a directory`;
}

function printHelp() {
  fs.readFileAsync(fileHelp).then(function(content) {
    console.log(JSON.parse(content).prompt);
  }).catch(function () {
    console.log(promptFileNotFound(fileHelp));
  });
}

function printVersion() {
  fs.readFileAsync(fileInfo).then(function(content) {
    const info = JSON.parse(content);
    console.log(`version: ${info.version}`);
    console.log(`author: ${info.author}`);
  }).catch(function() {
    console.log(promptFileNotFound(fileInfo));
  });
}

/*
 * given command line input, returns a parsed option
 * {_: [FILES], OPTION: Boolean, OPTIONAL_INPUT: F}
 */
function parse(input) {
  return parseArgs(input, {
    boolean: validOpts
  });
}

/*
 * given a parsed option, checks whether it contains invalid options
 */
function validate(parsedInput) {
  const validOptSet = new Set(validOpts);
  validOptSet.add(optionalInput);
  validOptSet.add(minimistChar); 
  let valid = true;
  Object.keys(parsedInput).forEach(function(key) {
    if (valid && !validOptSet.has(key)) {
      console.log(promptIllegal(key));
      console.log(promptUsage());
      valid = false;
    }
  });
  return valid;
}

/*
 * given a parsed option, returns an object flags which counts are computed
 * if none options are given, default to counting bytes, words, and lines
 */
function generateOpts(parsedInput) {
  const opts = {};
  if (parsedInput.c || parsedInput.bytes) {
    opts.byte = true;
  }
  if (parsedInput.m || parsedInput.chars) {
    opts.char = true;
  }
  if (parsedInput.w || parsedInput.words) {
    opts.word = true;
  }
  if (parsedInput.l || parsedInput.lines) {
    opts.line = true;
  }
  if (parsedInput.L || parsedInput['max-line-length']) {
    opts.maxLength = true;
  }
  if (!Object.keys(opts).length) {
    opts.byte = true;
    opts.word = true;
    opts.line = true;
  }
  return opts;  
}

/*
 * read utf-8 encoded string from standard in
 * TODO support other encodings
 */
function readStdin() {
  return new Promise(function(resolve) {
    let data = '';
    process.stdin.setEncoding(encoding);
    process.stdin.on('readable', function(){
      const chunk = process.stdin.read();
      if (chunk !== null) data += chunk;      
    });
    process.stdin.on('end', function() {
      resolve(data);
    });
  });
}

/*
 * given a list of file names and counting option, read their contents as utf-8
 * encoded string and count accordingly, returns an array of result 
 * [{file: String, counter: Object, error: [notFoundError, directoryError]}]
 * where counter is an optional object recording the counting result,
 * {byte: Number, word: Number, line: Number, char: Number, maxLength: Number}
 * TODO support other encodings
 */
function processFiles(files, opts) {
  return Promise.all(files.map(function (file) {
    // file could be a directory
    return fs.statAsync(file).then(function(stat) {
      if (stat.isFile()) {
        return fs.readFileAsync(file, encoding).then(function (content){
          return countContent(content, opts, file);
        }).catch(function(error) {
          return {file: error.cause.path, error: notFoundError};
        });        
      } else if (stat.isDirectory()) {
        return Promise.resolve({file: file, error: directoryError});
      }
    }).catch(function(error) {
      return Promise.resolve({file: error.cause.path, error: notFoundError});
    });
  })).then(formatOutput); 
}

/*
 * given string content, counting option, and an optional file name
 * count accordingly and returns a counter 
 * {byte: Number, word: Number, line: Number, char: Number, maxLength: Number}
 */
function countContent(content, opts, file) {
  const counter = {};
  if (opts.byte) {
    counter.byte = Buffer.byteLength(content, encoding);
  } 
  if (opts.char) {
    counter.char = content.length;
  }
  if (opts.word) {
    counter.word = content.split(spaceRegexp).map(function (ele) {
      return ele.trim();
    }).filter(function(ele) {
      return ele.length > 0;
    }).length;
  }
  if (opts.line) {
    counter.line = content.split(newlineRegexp).length - 1;
  }
  if (opts.maxLength) {
    counter.maxLength = content.split(newlineRegexp).map(function(line) {
      // TODO consider other non-fixed-width characters
      const tabbedArray = line.split(tabChar);
      const numTabs = tabbedArray.length - 1;
      return line.length - numTabs + numTabs * 8;
    }).reduce(function(memo, val) {
      return memo < val ? val : memo;
    }, 0);
  }
  return {counter: counter, file: file};  
}

/*
 * given an array of result returned by processFile(), return the formatted
 * output to be printed on console
 * NOTE 1) counts are padded at a global scale, i.e., to the widest count; this
 *      includes the total count (if present), which is different from the 
 *      implementation of GNU wc
 *      2) invalid entry with directory error output a zero count line       
 */
function formatOutput(entries) {
  
  if (!entries.length) return undefined;
  
  const keySequence = ['line', 'word', 'char', 'byte', 'maxLength'];
  const validEntries = entries.filter(function(entry) {
    return entry.counter;
  });  
  if (!validEntries.length) return entries.map(formatInvalidEntry).join('\n');
  const validEntry = validEntries[0];
  const validKeysInSequence = keySequence.filter(function(key) {
    return typeof validEntry.counter[key] != 'undefined';
  });  
  const printTotal = validEntries.length > 1;
  const printFile = !!validEntry.file;  
  const totalCounter = {};
  if (printTotal) computeTotalCounter();
  let maxColumn = computeMaxColumn();
  
  function computeTotalCounter() {
    validKeysInSequence.forEach(function(key) {
      const counts = validEntries.map(function(entry) {
        return entry.counter[key];
      });
      let count = 0;
      if (key === 'maxLength') {
        count = counts.reduce(function(memo, value) {
          return memo > value ? memo : value;
        }, 0);
      } else {
        count = counts.reduce(function(memo, value) {
          return memo + value;
        }, 0);
      }
      totalCounter[key] = count;
    });
  }
  
  function computeMaxColumn () {
    let counter = printTotal ? totalCounter : validEntries[0].counter;
    return Object.keys(counter).map(function(key) {
      return counter[key].toString().length;
    }).reduce(function(memo, value) {
      return memo < value ? value : memo;
    }, 0);
  }
  
  // if is a directory, print an addition 0 line
  function formatInvalidEntry(entry) {
    if (entry.error === notFoundError) {
      return promptFileNotFound(entry.file);
    } else if (entry.error === directoryError) {
      let row = `${promptDirectory(entry.file)}`;
      if (validEntries.length) {
        const zeroCounter = {};
        validKeysInSequence.forEach(function(key) {
          zeroCounter[key] = 0;
        });
        const zeroEntry = {file: entry.file, counter: zeroCounter};
        row += `\n${formatValidEntry(zeroEntry)}`;
      }
      return row;
    }    
  }
  
  function formatValidEntry(entry) {
    let row = validKeysInSequence.map(function(key) {
      return padLeft(entry.counter[key], maxColumn);
    }).join(spaceChar);
    if (printFile) {
      row += ` ${entry.file}`;
    }    
    return row;
  }
  
  function padLeft(value, width) {
    const padding = Array(width).fill(spaceChar).join('');
    return (padding + value).slice(-width);
  }  
  
  let output = entries.map(function(entry) {
    return entry.counter ? formatValidEntry(entry) : formatInvalidEntry(entry);
  }).join(newlineChar);
  if (printTotal) {
    const totalEntry = {counter: totalCounter, file: 'total'};
    output += newlineChar + formatValidEntry(totalEntry);
  }
  return output;    
}

/*
 * the main routine of wc, the workflow is as follows
 * 1) parse command line arguments
 * 2) check option validity
 * 3) generate counting options
 * 4) read inputs, there are 4 modes:
 *    a) a file list from standard input
 *    b) a file list from a file on disk
 *    c) specified files on disk
 *    d) content directly from standard input
 * 5) output results
 * 
 * NOTE --help option ovrrides all other options
 *      --version option overrides all counting options  
 *      allows arbitrary sequence of options and files, different from GNU wc    
 */
function execute() {
  const input = process.argv.slice(2);
  const parsedInput = parse(input);
  if (validate(parsedInput)) {
    if (parsedInput.help) {
      printHelp();
    }
    else if (parsedInput.version) {
      printVersion();
    }
    else {
      const opts = generateOpts(parsedInput);
      if (parsedInput[optionalInput]) {
        const filesFrom = parsedInput[optionalInput];
        let filesFromPromise = undefined;
        if (filesFrom === inputFromStdinChar) {
          // read a list of file names from standard in
          filesFromPromise = readStdin();
        } else {
          // read a list of file names from a specified file
          filesFromPromise = fs.readFileAsync(filesFrom, encoding);
        }
        filesFromPromise.then(function(content) {
          return content.split(nullChar).filter(function(file) {
            return file.length;
          });
        }).then(function(files) {
          return processFiles(files, opts);
        }).then(function(output) {
          if (output) console.log(output);
        }).catch(function() {
          console.log(promptFileNotFound(filesFrom));
        });
      } else {
        if (parsedInput[minimistChar].length) {
          // read content from specified files
          processFiles(parsedInput[minimistChar], opts)
            .then(function(output) {
              if (output) console.log(output);
            });     
        } else {
          // read content directly from standard in
          readStdin().then(function(content) {
            return countContent(content, opts);
          }).then(function(result){
            const output = formatOutput([result]);
            if (output) console.log(output);
          });
        }       
      }
    }
  }
}

execute();
// console.log(validate(parse(process.argv.slice(2))));

// for testing purpose only
module.exports = {
  promptIllegal: promptIllegal,
  promptDirectory: promptDirectory,
  promptUsage: promptUsage,
  promptFileNotFound: promptFileNotFound,
  parse: parse,
  validate: validate,
  generateOpts: generateOpts,
  processFiles: processFiles,
  countContent: countContent,
  formatOutput: formatOutput,
  execute: execute
};